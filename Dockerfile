FROM python

RUN apt update

RUN apt install tor -y
COPY config/torrc /etc/tor/torrc

RUN pip install --upgrade pip
ADD src/requirements.txt /src/app/requirements.txt
RUN pip install -r /src/app/requirements.txt

WORKDIR /src/app

CMD ["flask", "run"]
