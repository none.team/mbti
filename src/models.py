import csv
import json
import time
import asyncio
from aiohttp import ClientSession

from application import app


class DatasetGenerate:
    """ Генерирует фичи для датасета. В дальнейшем их буду отправлять
        в АПИ размечающего сервиса для получения меток.
    """

    def __init__(self, features_count, features_range, sparse_step=1e4):
        self._features_count = features_count
        self._features_range = features_range.split(",")
        self._sparse_step = sparse_step
        self._data = ["0" * self._features_count]

    def fill_data(self):
        """ Генерация данных в определенным шагом """
        max_int = int("1" * self._features_count, 2)
        for d in [max_int // int(self._sparse_step), int(1e3), int(1e2), int(1e1), int(1e0)]:
            while max_int - int(self._data[-1], 2) >= d:
                last = int(self._data[-1], 2)
                self._data.append("{:>60}".format(bin(last + d)[2:]).replace(" ", "0"))
        return self

    def show_path(self):
        """ Путь к сохраненному файлу(датасету) """
        path = "{}/16personalities_{}.csv".format(app.config.get("DATA_PATH"), time.time())
        with open(path, "w") as f:
            w = csv.writer(f)
            for r in self._data:
                w.writerow([ self._features_range[int(i)] for i in list(r) ])
        print(path)


class DatasetSetLabels:
    """
    """

    URL = "https://www.16personalities.com/ru/rezultaty-testa"

    def __init__(self, filename):
        self._filepath = "{}/{}".format(app.config.get("DATA_PATH"), filename)
        self._dataset = []
        self._body_template = {}

    def _load_dataset(self):
        """ Загрузка неразмеченного датасета """
        with open(self._filepath) as f:
            reader = csv.reader(f)
            for r in reader:
                self._dataset.append(list(r))

    def _load_body_template(self):
        """ Загрузка шаблона бодьи для обращения в размечающий сервис """
        with open("body-template.json") as f:
            self._body_template = json.loads(f.read())

    def _get_body(self, features):
        """ Заполнение шаблонного JSON данными из датасета """
        body = self._body_template
        for i, _ in enumerate(body.get("questions")):
            body.get("questions")[i]["answer"] = features[i]
        return body

    def __parse_response(self, value):
        """ Выделение класса из ответа сервиса """
        return value.decode()[1:-1].split("/")[-1]

    def __save_dataset(self):
        """ Сохранение датасета с загруженными лейблами """
        with open(self._filepath, "w") as f:
            w = csv.writer(f)
            for r in self._dataset:
                w.writerow(r)
            print("Dataset updated")

    async def fetch(self, i, body):
        """ Отправка запроса в размечающий сервис """
        headers = {
            "Content-Type": "application/json",
            "Host": "www.16personalities.com",
            "Content-Length": str(len(str(body))),
        }
        try:
            async with self.session.post(self.URL, data=json.dumps(body), headers=headers) as response:
                return i, await response.read()
        except Exception as e:
            return i, None

    async def manager(self):
        """ Управление обращениями в размечающий сервис """
        async with ClientSession() as session:
            self.session = session
            while True:
                tasks = []
                for i, features in enumerate(self._dataset):
                    if len(features) == 60:
                        body = self._get_body(features)
                        tasks.append(asyncio.create_task(self.fetch(i, body)))
                        await asyncio.sleep(0)
                    if len(tasks) == 10:
                        break
                if len(tasks) == 0:
                    break
                for i, response in await asyncio.gather(*tasks):
                    if response:
                        label = self.__parse_response(response)
                        self._dataset[i].append(label)
                        print(i, label, end="\r")

    def run(self):
        """ Основная логика асинхронных запросов """
        try:
            self._load_dataset()
            self._load_body_template()
            loop = asyncio.get_event_loop()
            future = asyncio.ensure_future(self.manager())
            loop.run_until_complete(future)
        except KeyboardInterrupt as e:
            self.__save_dataset()
        self.__save_dataset()
