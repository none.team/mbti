import os
import logging
import dotenv


dotenv.load_dotenv('.flaskenv')

logging.getLogger('stem').disabled = True

os.makedirs(os.path.dirname(os.getenv("DATA_PATH")), exist_ok=True)
os.makedirs(os.path.dirname(os.getenv("LOG_PATH")), exist_ok=True)

logging.basicConfig(
    filename="{}/app.log".format(os.getenv("LOG_PATH")),
    format="%(asctime)s - %(levelname)s %(message)s",
    level=logging.INFO
)


class Config(object):
    """
    """

    TOR = {
        "PROXIES": {
            "http": "{}://{}:{}".format(os.getenv("TOR_HTTP_PROTOKOL", "socks5h"),
                                        os.getenv("TOR_HTTP_HOST", "localhost"),
                                        os.getenv("TOR_HTTP_PORT", 9050)),
            "https": "{}://{}:{}".format(os.getenv("TOR_HTTPS_PROTOKOL", "socks5h"),
                                         os.getenv("TOR_HTTPS_HOST", "localhost"),
                                         os.getenv("TOR_HTTPS_PORT", 9050)),
        },
        "CONTROL_PORT": int(os.getenv("TOR_CONTROL_PORT", 9051))
    }

    DATA_PATH = os.getenv("DATA_PATH")

