import click
from flask.cli import AppGroup

from application import app
from models import DatasetGenerate, DatasetSetLabels


dataset = AppGroup("dataset")


@dataset.command("generate")
@click.argument("features_count", default=60)
@click.argument("features_range", default="-3,3")
def dataset_generate(features_count, features_range):
    """ Сгенерировать фичи для датасета. """
    DatasetGenerate(features_count, features_range).fill_data()\
                                                   .show_path()

@dataset.command("set-labels")
@click.argument("filename")
def dataset_set_labels(filename):
    """ Разметить лейблами датасет. """
    DatasetSetLabels(filename).run()

app.cli.add_command(dataset)
